-- Insterting/Creating Records
INSERT INTO artists (name) VALUES ("Rivermaya");

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-01-01", 1);

INSERT INTO songs (song_name, song_length, genre, album_id) VALUES ("Kundiman", 234, "OPM", 1);

INSERT INTO songs (song_name, song_length, genre, album_id) VALUES ("Kisapmata", 259, "OPM", 1);

/*
	Miniactivity: 10 min

		insert 1 record each for the ff tables:
			artists 
				album of the artist
					songs under the album

*/

INSERT INTO artists (name) VALUES ("Linkin Park");


INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Hybrid Theory", "2000-10-24", 2);


INSERT INTO songs (song_name, song_length, genre, album_id) VALUES ("Paper Cut", 305, "Rock", 2);

INSERT INTO songs (song_name, song_length, genre, album_id) VALUES ("Crawling", 323, "Rock", 2);


-- Selecting/Retrieving Records

-- display song_name of all songs
SELECT song_name FROM songs;

-- display all columns of songs table
SELECT * FROM songs;

-- display multiple columns of songs table (song_name & genre)
SELECT song_name, genre FROM songs;


-- display the title of all "OPM" songs
SELECT song_name FROM songs WHERE genre = "OPM";

-- WHERE can also be paired with AND and OR
SELECT song_name FROM songs WHERE genre = "Rock" AND song_length > 300;

SELECT * FROM songs WHERE genre = "Rock" OR song_length < 300;


-- Updating Records
UPDATE songs SET song_length = 240 WHERE song_name = "Kundiman";

-- removing "WHERE" will result into all columns being updated


-- Deleting Records
DELETE FROM songs WHERE genre = "OPM" AND song_length = 240;

-- removing "WHERE" will result to the deletion of ALL rows